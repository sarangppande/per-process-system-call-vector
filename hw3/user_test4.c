#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>		/* open */
#include <unistd.h>		/* exit */
#include <sys/ioctl.h>		/* ioctl */
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <linux/sched.h>
#include "sys_ioctl.h"
#include "syscall_override.h"

#define MAX_FILENAME 512
#define MAX_VECTOR_NAME_LEN 256

/*
 * Main - Call the ioctl functions
 */
int main(int argc, char **argv)
{
	int ret = 0;
	int number = 0;
	int child_id = -1;
	int file_desc, i;
	printf("My process ID : %d\n", getpid());
	sleep(2);
	ret = link("test_file1","test_file2");
	sleep(5);
	printf("returned from link  with ret=%d\n", ret);
	printf("\nSystem call done\n waiting for removing vector\n");
out:
	return 1;
}
