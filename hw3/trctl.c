#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/stat.h>
#include <linux/ioctl.h>
#include "trctl.h"
#include "sys_ioctl.h"

/*New Defines*/

#define ADD		"add"
#define REMOVE	"remove"

int main(int argc, char* argv[]) {

	int i, input_fd;
	static char use[] = "%s : usage= ./trctl CMD /mounted/path\n";
	char *add = "all";
	char *remove = "remove";
	char *cmd;
	char *path = "/dev/ioctl_device";
	int pid;
	char *action;
	int vector_id;
	struct ioctl_params params;
	unsigned int bitmap;
	int ret_val;
	if (argc < 2) {
		fprintf(stderr, "%s: please provide correct arguments\n", argv[0]);
		fprintf(stderr, use, argv[0]);
		exit(1);
	}
	input_fd = open(path, 0);
	if (input_fd < 0) {
		fprintf(stderr, "%s: Cannot find module\n",argv[0]);
		exit(0);
	}
	if (argc == 2) {
		pid = atoi(argv[1]);
		//memset(params.vec_name, 0, 256);
		params.pid = pid;
		params.bitmap = 1;
		params.vector_id = 0;
		ret_val = ioctl(input_fd, IOCTL_PROCESS_VECTOR, &params);
		if (ret_val < 0) {
			close(input_fd);
			fprintf(stderr, "%s: ioctl_get_msg failed:%d\n", argv[0],ret_val);
			exit(-1);
		}
		printf("Current vector set:%d\n", params.vector_id);
	} else {
		/*Normal behavior*/
		pid = atoi(argv[1]);
		action = argv[2];
		vector_id = atoi(argv[3]);
		bitmap = atoi(argv[4]);
		printf("bitmap = %x\n",bitmap);
		params.pid = pid;
		params.bitmap = bitmap;
		params.vector_id = vector_id;
		/*storing hex value*/
		if (strcmp(action, ADD) == 0){
			ret_val = ioctl(input_fd, IOCTL_VECTOR_SET, &params);
			if (ret_val < 0) {
				close(input_fd);
				fprintf(stderr, "%s: ioctl_get_msg failed:%d\n", argv[0],ret_val);
				exit(-1);
			}
		}
		else if (strcmp(action, REMOVE) == 0){
			ret_val = ioctl(input_fd, IOCTL_VECTOR_REMOVE, &params);
			if (ret_val < 0) {
				close(input_fd);
				fprintf(stderr, "%s: ioctl_get_msg failed:%d\n", argv[0],ret_val);
				exit(-1);
			}
		} else {
			fprintf(stderr, "%s: please provide correct arguments\n", argv[0]);
			fprintf(stderr, use, argv[0]);
			exit(1);
		}
	}
	close(input_fd);
	exit(1);
}
