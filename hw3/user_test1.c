#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>		/* open */
#include <unistd.h>		/* exit */
#include <sys/ioctl.h>		/* ioctl */
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <linux/sched.h>
#include "sys_ioctl.h"
#include "syscall_override.h"

#define MAX_FILENAME 512
#define MAX_VECTOR_NAME_LEN 256

/*
 * Main - Call the ioctl functions
 */
int main(int argc, char **argv)
{
	int ret = 0;
	int number = 0;
	int child_id = -1;
	int file_desc, i;
	int status = 0;
	pid_t wpid;
	printf("My process ID : %d\n", getpid());
	sleep(2);
	ret = syscall(329, SIGCHLD | CLONE_VFORK, 0, NULL, NULL, 0, 1);
	if(ret < 0) {
			printf("ERROR: clone2 system call failed \n");
			goto out;
	}
	//ret = mkdir("test_dir",777);
	printf("returned from clone  with ret=%d\n", ret);
	wpid = wait(&status);
	printf("\nSystem call done\n waiting for removing vector\n");
out:
	return 1;
}
