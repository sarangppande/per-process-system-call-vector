#ifndef SYS_IOCTL_H
#define SYS_IOCTL_H

#include <linux/ioctl.h>
#define DEVICE_NUM 131

#define IOCTL_VECTOR_SET _IOR(DEVICE_NUM, 1, char*)
#define IOCTL_VECTOR_REMOVE _IOR(DEVICE_NUM, 2, char*)
#define IOCTL_PROCESS_VECTOR _IOR(DEVICE_NUM, 3, char*)
#endif


#define MAX_VECTOR_NAME_LEN 256

struct ioctl_params{
	int pid;
	int vector_id;
	unsigned short bitmap;
};
