#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/syscalls.h>
#include <linux/mm.h>
#include <linux/unistd.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/err.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/moduleparam.h>
#include <linux/module.h>
#include <linux/stat.h>

#include "syscall_override.h"
#include "sys_ioctl.h"
#include "load_unload.h"

char *vector_name = "vector_1";
struct syscall_vector *vec_head = NULL;

extern int register_syscall(char *vec_name, unsigned long vec_address,
                struct module *vec_module,int vector_id);
extern int unregister_syscall(unsigned long vec_address);

asmlinkage long wrap_symlink(const char __user *file1, const char __user *file2) {
        printk(KERN_INFO "inside wrapped symlink system call");
        return 1;
}

asmlinkage long wrap_link(const char __user *file1, const char __user *file2) {
	printk(KERN_INFO "inside wrapped link system call");
	return 1;
}

asmlinkage long wrap_unlink(const char __user *file) {
	printk(KERN_INFO "inside wrapped unlink system call");
	return 1;
}

asmlinkage long wrap_write(int fd, const char __user *buf,
		size_t count) {
	printk(KERN_INFO "inside wrapped write system call");
	return 1;
}

asmlinkage long wrap_read(int fd, const char __user *buf, size_t count) {
    printk(KERN_INFO "inside wrapped read system call");
    return 1;
}

static int add_syscall_to_vector(int syscall_no, unsigned long func_ptr) {
	int ret = 0;
	struct syscall_vector *sys_vec = NULL;
	struct syscall_vector *temp = NULL;
	printk("add syscall to vector \n");
	sys_vec = (struct syscall_vector *)kmalloc(sizeof(struct syscall_vector),
					GFP_KERNEL);
	if(sys_vec == NULL) {
		ret = -ENOMEM;
		goto out;
	}
	if(IS_ERR(sys_vec)) {
		ret = PTR_ERR(sys_vec);
		goto out;
	}

	memset(sys_vec, 0, sizeof(struct syscall_vector));
	sys_vec->syscall_no = syscall_no;
	sys_vec->function_ptr = func_ptr;
	sys_vec->next = NULL;

	if(vec_head == NULL) {
		vec_head = sys_vec;
		goto out;
	}

	temp = vec_head;
	while(temp->next != NULL) {
		temp = temp->next;
	}

	temp->next = sys_vec;
out:
	return ret;
}

static void delete_syscall_from_vector(void) {
	struct syscall_vector *temp = NULL;
	struct syscall_vector *new_head = NULL;
	new_head = vec_head;

	if(vec_head == NULL) {
		goto out;
	}

	while(new_head->next != NULL) {
		temp = new_head;
		new_head = new_head->next;
		kfree(temp);
		temp = NULL;
	}
out:
	kfree(new_head);
	new_head = NULL;
	vec_head = NULL;
}

static int initialize_syscall_vector(void) {
        int ret = 0;
        int symlink_syscall_no = 88;
        int link_syscall_no = 86;
        int unlink_syscall_no = 87;
        int read_syscall_no = 0;
        int write_syscall_no = 1;

        printk("initialize sys call \n");
        ret = add_syscall_to_vector(symlink_syscall_no, (unsigned long)&wrap_symlink);
        if(ret < 0) {
                goto out;
        }
        ret = add_syscall_to_vector(link_syscall_no, (unsigned long)&wrap_link);
        if(ret < 0) {
                goto out;
        }
        ret = add_syscall_to_vector(unlink_syscall_no, (unsigned long)&wrap_unlink);
        if(ret < 0) {
                goto out;
        }
        ret = add_syscall_to_vector(read_syscall_no, (unsigned long)&wrap_read);
        if(ret < 0) {
                goto out;
        }
        ret = add_syscall_to_vector(write_syscall_no, (unsigned long)&wrap_write);
        if(ret < 0) {
                goto out;
        }
        ret = register_syscall(vector_name, (unsigned long)vec_head, THIS_MODULE,1);
out:
    return ret;
}

static int __init init_override(void)
{
	int ret = 0;
	ret = initialize_syscall_vector();
	if(ret < 0) {
			delete_syscall_from_vector();
	}
	return ret;
}

static void __exit cleanup_override(void)
{
	int ret;
	ret = unregister_syscall((unsigned long)vec_head);
	if(vec_head != NULL) {
			delete_syscall_from_vector();
	}
}

module_init(init_override);
module_exit(cleanup_override);
MODULE_LICENSE("GPL");
