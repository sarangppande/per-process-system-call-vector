#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/unistd.h>
#include <linux/semaphore.h>
#include <asm/cacheflush.h>
#include <asm/pgtable_types.h>
#include <linux/kallsyms.h>
#include <linux/linkage.h>
#include <linux/moduleloader.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include "syscall_override.h"

extern unsigned long get_vec_address(int vector_id);

asmlinkage extern long (*sysptr)( unsigned long clone_flags,
                        unsigned long newsp,
                        int __user * parent_tidptr,
                        int __user * child_tidptr,
                        unsigned long tls_val,
                        int id); 

asmlinkage long clone2( unsigned long clone_flags,
                        unsigned long newsp,
                        int __user * parent_tidptr,
                        int __user * child_tidptr,
                        unsigned long tls_val,
                        int id)
{

        int val=0, parent_id;
        struct task_struct *ctask = NULL;
        struct task_struct *ptask = NULL;
        struct syscall_override *s = NULL;
        struct syscall_vector *sys_vec = NULL;
	printk("reaching here\n");
        parent_id = task_pid_nr(current);
	ptask = pid_task(find_vpid(parent_id), PIDTYPE_PID);
	printk("parent process pid =%d, and vector id set for it =%d\n", parent_id,
			((struct syscall_override*)ptask->override_syscall)->vector_id);
        s = kmalloc(sizeof(struct syscall_override), GFP_KERNEL);
        if (s == NULL){
                val = -ENOMEM;
                printk("could not assign memory to S \n");
		goto out;
        }
        sys_vec = (struct syscall_vector *)get_vec_address((int)id);
                if(sys_vec == NULL) {
			printk("sys_vec NULL \n");

                       val = -EINVAL;

                        goto out;
                }
        s->vector_id = (int)id;
        s->vector_head = sys_vec;
        printk("input vector id: %d\n", id);
        val =  _do_fork(clone_flags, newsp, 0, NULL, NULL, 0);
        printk("child pid =%d \n", val);
        rcu_read_lock();
	ctask = pid_task(find_vpid(val), PIDTYPE_PID);
        if (ctask != NULL) {
                ctask->override_syscall = (void*)s;
        }
        rcu_read_unlock();
	printk("child process pid =%d, and vector id set for it =%d\n", val,
			((struct syscall_override*)ctask->override_syscall)->vector_id);
out:
        return val;

}
static int __init init_sys_clone2(void)
{
        printk("installed new sys_clone2 module\n");
        if (sysptr == NULL)
                sysptr = clone2;
        return 0;
}
static void  __exit exit_sys_clone2(void)
{
        if (sysptr != NULL)
                sysptr = NULL;
        printk("removed sys_clone2 module\n");
}
module_init(init_sys_clone2);
module_exit(exit_sys_clone2);
MODULE_LICENSE("GPL");

