#ifndef __REG_VECTOR_H_
#define __REG_VECTOR_H_

#define MAX_VECTOR_NAME_LEN 256
#define MAX_BUFFER_SIZE 4096

struct sys_vector {
	char vec_name[MAX_VECTOR_NAME_LEN];
	unsigned long vec_address;
	int ref_count;	
	struct module *vec_module;
	struct sys_vector *next;
	int vector_id;
};

#endif

