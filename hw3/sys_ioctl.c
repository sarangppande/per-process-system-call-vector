#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/sched.h>
#include <linux/mutex.h>
#include <linux/slab.h>
#include <linux/module.h>

#include "syscall_override.h"
#include "sys_ioctl.h"
#define DEVICE_NAME "ioctl_device"

extern unsigned long get_vec_address(int vector_id);
extern int decrease_ref_count(int vector_id);
struct mutex ioctl_lock;

static int add_to_task(int pid,struct syscall_override *sys_vec) {
	int ret = -1;
	struct task_struct *task = NULL;
	rcu_read_lock();
	task = pid_task(find_vpid(pid), PIDTYPE_PID);
	if(task != NULL && task->override_syscall != NULL) {
		printk(KERN_INFO "Process found with vector id : %d",
				((struct syscall_override *)
				task->override_syscall)->vector_id);
		decrease_ref_count(((struct syscall_override*)
				task->override_syscall)->vector_id);
		ret = 0;
		task->override_syscall = (void *)sys_vec;
	}
	else if(task != NULL && task->override_syscall == NULL) {
		printk(KERN_INFO "Process found");
		ret = 0;
		task->override_syscall = (void *)sys_vec;
	}else{
		printk(KERN_INFO "Process not found");
	}
	rcu_read_unlock();
	return ret;
}

static int get_from_task(struct ioctl_params *params) {
	int ret = -1;
	struct task_struct *task = NULL;
	rcu_read_lock();
	printk(KERN_INFO "\nPID: %d\n",params->pid);
	task = pid_task(find_vpid(params->pid), PIDTYPE_PID);
	if(task !=NULL && task->override_syscall != NULL) {
		printk(KERN_INFO "Process found");
		ret = 0;
		params->vector_id = ((struct syscall_override *)
				task->override_syscall)->vector_id;
	}else{
		printk(KERN_INFO "Process not found");
	}
	rcu_read_unlock();
	return ret;
}

static void remove_from_task(struct ioctl_params *params) {
	struct task_struct *task = NULL;
	int ret = -1;
	rcu_read_lock();
	task = pid_task(find_vpid(params->pid), PIDTYPE_PID);
	if( task != NULL && task->override_syscall != NULL) {
		printk(KERN_INFO "Process found");
		ret = decrease_ref_count(((struct syscall_override*)
				task->override_syscall)->vector_id);
		printk(KERN_INFO "Ref Count: %d", ret);
		kfree(task->override_syscall);
		task->override_syscall = NULL;
	}
	rcu_read_unlock();
}

static long ioctl_device(struct file *file, unsigned int ioctl_num,
		unsigned long params) {
	int ret;
	struct ioctl_params *temp, *p;
	struct syscall_override *s = NULL;
	struct syscall_vector *sys_vec;

	mutex_lock(&ioctl_lock);
	ret = 0;
	sys_vec = NULL;
	temp = (struct ioctl_params *)params;
	p = kmalloc(sizeof(struct ioctl_params), GFP_KERNEL);
	if(p == NULL) {
		ret = -ENOMEM;
		goto out;
	}
	s = kmalloc(sizeof(struct syscall_override), GFP_KERNEL);
	if(s == NULL) {
		ret = -ENOMEM;
		goto out;
	}

	switch(ioctl_num) {
	case IOCTL_VECTOR_SET:
		printk(KERN_INFO "Setting vector for process");
		ret = copy_from_user(p, temp, sizeof(struct ioctl_params));
		if(ret < 0) {
			ret = -EINVAL;
			goto out;
		}

		sys_vec = (struct syscall_vector *)get_vec_address(p->vector_id);
		if(sys_vec == NULL && p->vector_id != 0) {
			ret = -EINVAL;
			goto out;
		}
		else {
			s->vector_id = p->vector_id;
			s->vector_head = sys_vec;
			s->bitmap = p->bitmap;
			ret = add_to_task(p->pid,s);
		}
		if(ret < 0) {
			decrease_ref_count(p->vector_id);
		}
		printk(KERN_INFO "Setting vector for process successful");
		break;

	case IOCTL_VECTOR_REMOVE:
		printk(KERN_INFO "Removing vector for process");
		ret = copy_from_user(p, temp, sizeof(struct ioctl_params));
		if(ret < 0) {
			ret = -EINVAL;
			goto out;
		}

		remove_from_task(p);
		printk(KERN_INFO "Removed vector for process");
		break;

	case IOCTL_PROCESS_VECTOR:
		printk(KERN_INFO "Get vector for process");
		ret = copy_from_user(p, temp, sizeof(struct ioctl_params));
		printk("PID:%d",p->pid);
		if(ret < 0) {
			ret = -EINVAL;
			goto out;
		}

		ret = get_from_task(p);
		if(ret < 0) {
			ret = -EINVAL;
			goto out;
		}

		ret = copy_to_user((struct ioctl_params *)params,p,sizeof(struct ioctl_params));
		if(ret < 0) {
			ret = -EINVAL;
			goto out;
		}
		break;
		printk(KERN_INFO "getting vector for process done");
	default:
		ret = -1;
		break;
	}
	printk("Reached here");
	if (p != NULL){
		kfree(p);
	}
	mutex_unlock(&ioctl_lock);
	printk("Returning");
	return (long)ret;
out:
	if (p != NULL){
		kfree(p);
	}
	if (s != NULL){
		kfree(s);
	}
	mutex_unlock(&ioctl_lock);
	return (long)ret;
}

struct file_operations fops = {
		.unlocked_ioctl = ioctl_device,
};

static int __init init_ioctl_module(void)
{
	int ret = 0;
	ret = register_chrdev(DEVICE_NUM, DEVICE_NAME, &fops);
	if(ret < 0) {
		printk(KERN_ALERT "%s Device registeration failed with %d",
				DEVICE_NAME, ret);
		goto out;
	}
	mutex_init(&ioctl_lock);
out:
	return ret;
}

static void __exit exit_ioctl_module(void)
{
	unregister_chrdev(DEVICE_NUM, DEVICE_NAME);
}

module_init(init_ioctl_module);
module_exit(exit_ioctl_module);
MODULE_LICENSE("GPL");
