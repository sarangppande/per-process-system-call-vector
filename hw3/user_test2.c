#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>		/* open */
#include <unistd.h>		/* exit */
#include <sys/ioctl.h>		/* ioctl */
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <linux/sched.h>
#include "sys_ioctl.h"
#include "syscall_override.h"
#define CLONE_SYSCALLS  0x00001000
#define MAX_FILENAME 512
#define MAX_VECTOR_NAME_LEN 256

/*
 * Main - Call the ioctl functions
 */
clone_body(void *arg) {
        int ret1 = 0;
        printf("Child process ID : %d\n", getpid());
        //ret1 = open("test_clone", 66, 77);
	ret1 = mkdir("test_dir2",777);
        printf("child return value is: %d\n\n", ret1);
        _exit(0);
}



int main(int argc, char **argv)
{
	int ret = 0;
	int number = 0;
	int child_id = -1;
	int status =0;
	int file_desc, i;
	pid_t wpid;
	void **stack = NULL;

	printf("My process ID : %d\n", getpid());
	sleep(1);
	stack = (void**)malloc(65536);
        printf("Calling clone .. \n");


        ret = clone(&clone_body, stack+6553, SIGCHLD | CLONE_SYSCALLS | CLONE_FILES | CLONE_VM, NULL);
        if(ret < 0) {
                printf("ERROR1:  Ret of clone is negative\n\n");
        }

        printf("returned from clone ret=%d\n", ret);
        wpid = wait(&status);

//	ret = mkdir("test_dir",777);
	printf("System call done");
	sleep(7);
out:
	return ret;
}
