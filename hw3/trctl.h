#ifndef TRCTL_H
#define TRCTL_H

#define MAJOR_NUM 100
#define IOCTL_SET_MSG _IOR(MAJOR_NUM, 0, unsigned short)
#define IOCTL_GET_MSG _IOR(MAJOR_NUM, 1, unsigned short)

/* operation mapping*/
#define TRFS_NONE		0x0000
#define TRFS_WRITE		0x0001
#define TRFS_READ		0x0002
#define TRFS_OPEN		0x0004
#define TRFS_CREATE		0x0008
#define TRFS_LINK		0x0010
#define TRFS_UNLINK		0x0020
#define TRFS_SYMLINK	0x0040
#define TRFS_MKDIR		0x0080
#define TRFS_RMDIR		0x0100
#define TRFS_LSEEK		0x0200
//#define TRFS_UNLINK		0x0020
#define TRFS_ALL		0xffff

#endif
