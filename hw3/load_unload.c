#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/fs.h>
#include <asm/uaccess.h>        /* for get_user and put_user */
#include <linux/string.h>
#include <linux/mutex.h>
#include <linux/slab.h>

#include "load_unload.h"
#define AUTHOR "FSL"
#define DESCRIPTION "LOAD_UNLOAD"

static const char file_name[]="syscall_vectors";
struct proc_dir_entry* pde=NULL;
struct sys_vector * head=NULL;
char buffer[MAX_BUFFER_SIZE];
struct mutex vec_lock;

static ssize_t read_proc(struct file *filp, char
*buf, size_t count,loff_t *offp) {

        int ret;
        char *current_ptr;
        struct sys_vector *temp;
        int vec_len;
        ret = 0;
        temp = NULL;
        current_ptr = NULL;
        if(*offp>0) {
                ret=0;
                goto out;
        }
        memset(buffer,0,MAX_BUFFER_SIZE);
        current_ptr = buffer;
        if(head == NULL) {
                goto out;
        }
        temp = head;
        while ((temp != NULL) && (ret < MAX_BUFFER_SIZE)) {
                vec_len = strlen(temp->vec_name);
                memcpy(current_ptr, temp->vec_name,vec_len);
                current_ptr[vec_len]='\n';
                current_ptr = current_ptr + vec_len + 1;
                ret = ret + vec_len + 1;
                *offp = *offp + ret;
                temp = temp->next;
        }

        memcpy(buf, buffer, ret);
out:
        return ret;
}

static int add_vector(char *vec_name , unsigned long
vec_address, struct module *vector_module, int vector_id) {
        int ret = 0;
        struct sys_vector *v = NULL;
        struct sys_vector *temp = NULL;
        v = (struct sys_vector*)kmalloc(sizeof(struct
        sys_vector), GFP_KERNEL);
        if(v == NULL) {
                ret = -ENOMEM;
                goto out;
        }
        if(IS_ERR(v)) {
                ret = PTR_ERR(v);
                goto out;
        }

        memset(v, 0, sizeof(struct sys_vector));
        memcpy(v->vec_name, vec_name, strlen(
        vec_name));
        v->vector_id = vector_id;
        v->vec_address = vec_address;
        v->vec_module=vector_module;
        v->ref_count = 0;
        v->next = NULL;

        if(head == NULL) {
                head = v;
                goto out;
        }

        temp = head;
        while(temp->next != NULL) {
                temp = temp->next;
        }
        temp->next = v;

out:
        return ret;
}

static int remove_vector(unsigned long vec_address) {
        int ret=0;
        int flag=0;
        struct sys_vector *v=NULL;
        struct sys_vector *temp= NULL;
        if(head == NULL) {
                printk(KERN_INFO "List Head is null");
                ret=-EFAULT;
                goto out;
        }
        else if((head->next == NULL) && (head->vec_address == vec_address)) {
                v=head;
                goto check_ref_count;
        }
        else if(head->next != NULL && head->vec_address != vec_address) {
                temp=head;
                v=temp->next;
                while(v!=NULL) {
                        if(v->vec_address==vec_address) {
                                flag=1;
                                break;
                        }
                        v=v->next;
                        temp=v;
                }
        }
        else if(head->next != NULL && head->vec_address == vec_address) {
                v = head;
                goto check_ref_count;
        }
        else
                ;

        if(flag==0) {
                ret=-EFAULT;
                goto out;
        }
check_ref_count:
        if(v->ref_count>0) {
                ret=-1;
                goto out;
        }
        if(v!=head)
                temp->next=v->next;
        else
                head = head->next;
        kfree(v);
        v=NULL;
out:
        return ret;
}


int register_syscall(char *vec_name, unsigned long
vec_address, struct module *vector_module, int vector_id) {
        int ret=0;
        mutex_lock(&vec_lock);
        ret= add_vector(vec_name , vec_address,
        vector_module,vector_id);
        if(ret<0) {
                goto out;
        }

out:
        mutex_unlock(&vec_lock);
        return ret;
}
EXPORT_SYMBOL(register_syscall);

int unregister_syscall(unsigned long vec_address) {
        int ret=0;
        mutex_lock(&vec_lock);
        ret=remove_vector(vec_address);
        if(ret<0) {
                goto out;
        }

out:
        mutex_unlock(&vec_lock);
        return ret;
}
EXPORT_SYMBOL(unregister_syscall);

unsigned long get_vec_address(int vector_id) {
        struct sys_vector *temp=NULL;
        unsigned long v=0L;
        mutex_lock(&vec_lock);
        if(head == NULL) {
                goto out;
        }
        temp=head;
        while(temp!=NULL) {
                if(temp->vector_id == vector_id){
                                break;
                }
                temp=temp->next;
        }
        if(temp!=NULL) {
                temp->ref_count=temp->ref_count +1;
                try_module_get(temp->vec_module);
                v = temp->vec_address;
        }
out:
        mutex_unlock(&vec_lock);
        return v;
}
EXPORT_SYMBOL(get_vec_address);

int decrease_ref_count(int vector_id) {
        int ref_cnt=-1;
        struct sys_vector *temp=NULL;
        mutex_lock(&vec_lock);
        if(head==NULL) {
                goto out;
        }
        temp=head;
        while(temp!=NULL) {
        	if(temp->vector_id == vector_id){
        		break;
        	}
        	temp=temp->next;
        }
        if(temp!=NULL) {
                temp->ref_count=temp->ref_count-1;
                module_put(temp->vec_module);
                ref_cnt=temp->ref_count;
        }
out:
        mutex_unlock(&vec_lock);
        return ref_cnt;
}

EXPORT_SYMBOL(decrease_ref_count);

static const struct file_operations proc_file_ops={
        .owner = THIS_MODULE,
        .read = read_proc,
};

int init_module(void)
{
        int ret=0;
        pde=proc_create(file_name,0444,NULL,
        &proc_file_ops);
        if(IS_ERR(pde)) {
                ret=PTR_ERR(pde);
                goto out;
        }
                mutex_init(&vec_lock);
out:
        return ret;
}

void cleanup_module(void){
        remove_proc_entry(file_name,0);
}
MODULE_LICENSE("GPL");
MODULE_AUTHOR(AUTHOR);
MODULE_DESCRIPTION(DESCRIPTION);
