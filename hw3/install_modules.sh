#!/bin/sh
set -x
rmmod clone2.ko
rmmod sys_ioctl
rmmod vector_1
rmmod vector_2
rmmod load_unload
insmod load_unload.ko
insmod vector_1.ko
insmod vector_2.ko
insmod sys_ioctl.ko
insmod clone2.ko
mknod /dev/ioctl_device c 131 200
