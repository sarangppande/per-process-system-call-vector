#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/syscalls.h>
#include <linux/mm.h>
#include <linux/unistd.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/err.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/moduleparam.h>
#include <linux/module.h>

#include "syscall_override.h"
#include "sys_ioctl.h"

char *vector_name = "vector_2";
struct syscall_vector *vec_head = NULL;

extern int register_syscall(char *vec_name, unsigned long vec_address,
                struct module *vec_module,int vector_id);
extern int unregister_syscall(unsigned long vec_address);

asmlinkage long wrap_mkdir(const char __user *dir, umode_t mode ) {
	/*negative error value*/
        printk(KERN_INFO "inside wrapped mkdir system call");
        return -3636;
}

asmlinkage long wrap_rmdir(const char __user *dir) {
	/*negative error value*/
        printk(KERN_INFO "inside wrapped rmdir system call");
        return -3636;
}

asmlinkage long wrap_creat(const char __user *pathname, umode_t mode) {
	/*only logging : original system call called */
        printk(KERN_INFO "inside wrapped create system call");
        return -3636;
}

asmlinkage long wrap_close(const char __user *file) {
	/* only logging: original system call called */
        printk(KERN_INFO "inside wrapped close system call");
        return 1;
}


asmlinkage long wrap_chown(unsigned int fd, uid_t user, gid_t group) {
	/* only logging: original system call called */
        printk(KERN_INFO "inside wrapped chown system call");
        return -3636;
}
static int add_syscall_to_vector(int syscall_no, unsigned long func_ptr) {
        int ret = 0;
        struct syscall_vector *sys_vec = NULL;
        struct syscall_vector *temp = NULL;

        sys_vec = (struct syscall_vector *)kmalloc(sizeof(struct syscall_vector),
                        GFP_KERNEL);
        if(sys_vec == NULL) {
                ret = -ENOMEM;
                goto out;
        }
        if(IS_ERR(sys_vec)) {
                ret = PTR_ERR(sys_vec);
                goto out;
        }

        memset(sys_vec, 0, sizeof(struct syscall_vector));
        sys_vec->syscall_no = syscall_no;
        sys_vec->function_ptr = func_ptr;
        sys_vec->next = NULL;

        if(vec_head == NULL) {
                vec_head = sys_vec;
                goto out;
        }

        temp = vec_head;
        while(temp->next != NULL) {
                temp = temp->next;
        }

        temp->next = sys_vec;
out:
        return ret;
}


static void delete_syscall_from_vector(void) {
        struct syscall_vector *temp = NULL;
        struct syscall_vector *new_head = NULL;
        new_head = vec_head;

        if(vec_head == NULL) {
                goto out;
        }

        while(new_head->next != NULL) {
                temp = new_head;
                new_head = new_head->next;
                kfree(temp);
                temp = NULL;
        }
out:
        kfree(new_head);
        new_head = NULL;
        vec_head = NULL;
}

static int initialize_syscall_vector(void) {
	int ret = 0;
	int mkdir_syscall_no = 83;
	int rmdir_syscall_no = 84;
	int create_syscall_no = 85;
	int close_syscall_no = 3;
	int chown_syscall_no = 92;

	ret = add_syscall_to_vector(mkdir_syscall_no, (unsigned long)&wrap_mkdir);
	if(ret < 0) {
		goto out;
	}
	ret = add_syscall_to_vector(rmdir_syscall_no, (unsigned long)&wrap_rmdir);
	if(ret < 0) {
		goto out;
	}
	ret = add_syscall_to_vector(create_syscall_no, (unsigned long)&wrap_creat);
	if(ret < 0) {
		goto out;
	}
	ret = add_syscall_to_vector(close_syscall_no, (unsigned long)&wrap_close);
	if(ret < 0) {
		goto out;
	}
	ret = add_syscall_to_vector(chown_syscall_no, (unsigned long)&wrap_chown);
	if(ret < 0) {
		goto out;
	}
	ret = register_syscall(vector_name, (unsigned long)vec_head, THIS_MODULE,2);
out:
	return ret;

}


static int __init init_override(void)
{
	int ret = 0;
	ret = initialize_syscall_vector();
	if(ret < 0) {
			delete_syscall_from_vector();
	}
	return ret;
}

static void __exit cleanup_override(void)
{
	int ret;
	ret = unregister_syscall((unsigned long)vec_head);
	if(vec_head != NULL) {
			delete_syscall_from_vector();
	}
}

module_init(init_override);
module_exit(cleanup_override);
MODULE_LICENSE("GPL");
