#ifndef __SYSCALL_OVERRIDE_H_
#define __SYSCALL_OVERRIDE_H_

#ifndef MAX_VECTOR_NAME_LEN
#define MAX_VECTOR_NAME_LEN 256
#endif

struct syscall_vector {
	int syscall_no;
	unsigned long function_ptr;
	struct syscall_vector *next;	
};

struct syscall_override {
	int vector_id;
	struct syscall_vector *vector_head;
	unsigned short bitmap;
};


#endif

