
struct syscall_vector {
	int syscall_no;
	unsigned long function_ptr;
	struct syscall_vector *next;	
};

struct syscall_override {
	int vector_id;
	struct syscall_vector *vector_head;
	unsigned short bitmap;
};
